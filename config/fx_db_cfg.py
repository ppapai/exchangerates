from fx_db_wrapper.sqlite_fx_wrapper import SQLiteFXWrapper


DBs = {
    'sqlite': {
        'class': SQLiteFXWrapper,
        'params': {
            'location': 'sqlite_dbs',
            'db_name': 'fx_rates.db'
        }
    }
}
