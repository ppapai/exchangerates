from fx_pull_wrappers.fixer_fx_wrapper import FixerFXWrapper
from fx_pull_wrappers.openexchange_fx_wrapper import OpenexchangeFXWrapper
from fx_pull_wrappers.openrates_fx_wrapper import OpenratesFXWrapper

PROVIDERS = {
    'fixer': {
        'name': 'fixer',
        'class': FixerFXWrapper,
        'params': {
            'access_key': 'a10aae649786527a17934fbf14f8acd3',
            'endpoint': 'http://data.fixer.io/api/',
            }
    },
    'openexchange': {
        'class': OpenexchangeFXWrapper,
        'params': {
            'app_id': '5184fc5441c04274baf0410ed73840e4',
            'endpoint': 'https://openexchangerates.org/api/historical/'
        }
    },
    'openrates': {
        'class': OpenratesFXWrapper,
        'params': {
            'endpoint': 'http://api.openrates.io/'
        }
    }
}
