import pandas as pd
from abc import ABC, abstractmethod
from typing import *


class FXDBWrapperBase(ABC):

    def __init__(self, table_name: str, config: dict = None):
        self._table_name = table_name
        if config is None:
            from config.fx_db_cfg import DBs
            self._config = DBs[self.name()]['params']
        else:
            self._config = config

    @classmethod
    @abstractmethod
    def name(cls):
        pass

    @abstractmethod
    def upsert_data(self, dates: List[str], exchange_rates: List[dict]):
        pass

    @abstractmethod
    def read_data(self, start_date: str, end_date: str, currency: str) -> pd.Series:
        pass
