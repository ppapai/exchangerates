import sqlite3
import os

import pandas as pd

from typing import List

from fx_db_wrapper.fx_db_wrapper_base import FXDBWrapperBase


class SQLiteFXWrapper(FXDBWrapperBase):

    NAME = "sqlite"

    TABLE_CREATION_QUERY = """CREATE TABLE IF NOT EXISTS {} (
    date CHARACTER(20) NOT NULL,
    currency CHARACTER(20) NOT NULL,
    fx_rate REAL,
    PRIMARY KEY (date, currency)
    );"""

    INSERT_QUERY = """INSERT OR REPLACE INTO {}(date, currency, fx_rate) VALUES(?,?,?)"""

    READ_QUERY = """SELECT * FROM {} WHERE date between '{}' and '{}' and currency = '{}'"""

    @classmethod
    def name(cls):
        return cls.NAME

    def upsert_data(self, dates: List[str], exchange_rates: List[dict]):
        if not os.path.exists(self._config['location']):
            os.mkdir(self._config['location'])

        conn = sqlite3.connect(os.path.join(self._config['location'], self._config['db_name']))
        conn.execute(SQLiteFXWrapper.TABLE_CREATION_QUERY.format(self._table_name))

        for date, er in zip(dates, exchange_rates):
            usd = er['USD']  # need USD
            for cur, rate in er.items():
                conn.execute(SQLiteFXWrapper.INSERT_QUERY.format(self._table_name), (date, cur, rate / usd))

        conn.commit()
        conn.close()

    def read_data(self, start_date: str, end_date: str, currency: str) -> pd.Series:
        conn = sqlite3.connect(os.path.join(self._config['location'], self._config['db_name']))
        df = pd.read_sql_query(SQLiteFXWrapper.READ_QUERY.format(self._table_name, start_date, end_date, currency),
                               conn, index_col='date')
        return df['fx_rate'].sort_index()



