from abc import ABC, abstractmethod


class FXWrapperBase(ABC):

    def __init__(self, config: dict = None):
        if config is None:
            import config.fx_provider_cfg as cfg
            self._config = cfg.PROVIDERS[self.name()]['params']
        else:
            self._config = config

    @classmethod
    @abstractmethod
    def name(cls):
        pass

    @abstractmethod
    def get_historical_data(self, date: str):
        pass
