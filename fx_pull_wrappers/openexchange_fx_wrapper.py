import json
import logging
import urllib.parse
import urllib.request

from fx_pull_wrappers.fx_wrapper_base import FXWrapperBase


class OpenexchangeFXWrapper(FXWrapperBase):

    NAME = 'openexchange'

    @classmethod
    def name(cls):
        return cls.NAME

    def get_historical_data(self, date: str):
        variables = {
            'app_id': self._config['app_id']
        }

        urlencoding = urllib.parse.urlencode(variables)
        url = self._config['endpoint'] + date + '.json?' + urlencoding

        try:
            conn = urllib.request.urlopen(url)
            resp = conn.read()
        except Exception as e:
            logging.error("Failed to create connection!")
            raise e

        try:
            data = json.loads(resp)
        except Exception as e:
            logging.error(f"Wrong data format: {resp}")
            raise e

        return date, data['rates']

