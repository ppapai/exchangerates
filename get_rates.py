import sys
import pandas as pd

from config.fx_db_cfg import DBs
from config.fx_provider_cfg import PROVIDERS


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Please give the first date and the last date you want to collect exchange rates for.")

    start_date = sys.argv[1]
    end_date = sys.argv[2]
    if len(sys.argv) > 3:
        providers = sys.argv[3:]
    else:
        providers = PROVIDERS.keys()

    date_range = pd.date_range(start_date, end_date)
    for name in providers:
        provider = PROVIDERS[name]
        print(f"\n#####################\nDownloading from {name}\n", flush=True)
        fxConn = provider['class']()
        dates = []
        rates = []
        for d in date_range:
            print("Collecting data for " + str(d.date()) + "\n", flush=True)
            
            try:
                date, rate = fxConn.get_historical_data(str(d.date()))
                dates.append(date)
                rates.append(rate)
            except Exception as e:
                print(f"Exception occured, skipped {d}!")

        dbConn = DBs['sqlite']['class'](name)
        dbConn.upsert_data(dates, rates)

        #verify if its persisted:
        print("\nEUR/USD: \n")
        print(dbConn.read_data(start_date, end_date, currency='EUR'))
