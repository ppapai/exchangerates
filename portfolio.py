import numpy as np
import pandas
from scipy.optimize import LinearConstraint, minimize, Bounds
from fx_db_wrapper.sqlite_fx_wrapper import SQLiteFXWrapper

# try to allocate X amount of USD efficiently among several currencies

dbConn = SQLiteFXWrapper('openrates')
currencies = ['EUR', 'JPY', 'GBP', 'CHF', 'AUD', 'CAD', 'SGD']
print(f"Suggested allocation for {','.join(currencies)} currencies is being calculated.")
n_currencies = len(currencies)
start_date = '2011-01-01'
end_date = '2011-12-31'

# getting the daily change in exchange rates, the time series are normalized by USD
currency_ts = np.array([dbConn.read_data(start_date, end_date, currency).diff(1).iloc[1:].values
                        for currency in currencies])

# calculating the covariance matrix and the mean of the returns
cov = np.cov(currency_ts)
mus = np.mean(currency_ts, axis=1)

# the constraints of the optimization problem, take a look at
# https://www.mathworks.com/help/finance/portfolio.estimatemaxsharperatio.html
linear_const = LinearConstraint(mus.reshape((-1, n_currencies)), [1], [1])
bounds = Bounds([0] * n_currencies, [np.inf] * n_currencies)


# the sharp ratio: expected return / variance of the return
def sharp(x):
    return (mus @ x) / np.sqrt(x @ (cov @ x))


# defining the objective function and its derivatives for the solver
def f(x):
    return x @ (cov @ x) / 2


def f_prime(x):
    return cov @ x


def f_hess(x):
    return cov


# solving the problem

x0 = np.array([0.1] * n_currencies)
res = minimize(f, x0, method='trust-constr', jac=f_prime, hess=f_hess,
               constraints=[linear_const], tol=1e-16,
               options={'verbose': 1}, bounds=bounds)

solution = res.x / np.sum(res.x)


# double checking the solution with random search, inaccurate if there are a lot of currencies
solution_rand = x0
max_f = sharp(x0)

for x in np.random.rand(100000, n_currencies):
    x /= np.sum(x)
    eval_f = sharp(x)
    if max_f < eval_f:
        solution_rand = x
        max_f = eval_f


# comparing solutions: random vs optimizer

allocation_df = pandas.DataFrame(data={'currency': currencies, 'allocation_rnd': solution_rand, 'allocation': solution})
allocation_df.set_index('currency', inplace=True)

print(allocation_df)
print(f"Sharp ratios (random search --> optimizer): {sharp(solution_rand):.3} --> {sharp(solution):.3}")

# the solution seems to be a 2:1 split between EUR and CAD

