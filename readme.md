## Task for HonestBank

#### Requirements
I tested it with python 3.6 and 3.8. I use sqlite3, urllib, pandas 
for pulling, storing, and reading back the data. For the extra task, 
numpy and scipy are also required.

#### Approach
Wrapped the exchange rate providers with a simple API that queries them 
for historical data for a given date. The providers can be configured 
in fx_provider_cfg.py. For additional providers, you need to implement 
FXWrapperBase and define the appropriate config values.

For persistent storage, I use a SQLite which comes with python. 
The data is stored per provider in tables with a schema of 

(date CHARACTER(20) NOT NULL,
 
 currency CHARACTER(20) NOT NULL,
 
 fx_rate REAL)
 
 The fx_rate is CURRENCY/USD.
 
 If you want to use a different db, you need to implement FXDBWrapperBase. 
 Accessing the time series should be done through the db wrapper. 
 The data is not stored as time series natively. 
 
 The parameters about the location of the sqlite db can be changed in the 
 fx_db_cfg.py.
 
 Usage example:
 
 python get_rates.py 2011-01-01 2011-01-05
 
 This would query all 3 providers between those dates. (I  will run out of free
 api calls soon for 2 providers.)
 
 Or 
 
 python get_rates.py 2011-01-01 2011-01-05 openrates
 
 This would query only the OpenRates API.
 
 #### Extra
 Prerequisite: download some data:
 
 python get_rates.py 2011-01-01 2011-12-31 openrates
 
 I made a script that pulls values from the db for a few currencies 
 and calculates an allocation that maximizes the Sharp ratio 
 (expected return over standard deviation) of a portfolio 
 of the given currencies. 
 I used scipy.optimize to solve the constraint  optimization problem 
 and I verified its correctness by comparing it to brute force search 
 in a simple case.
  
 It is more pedagogical than useful. Please take a look at the inner details,
 I added comments.
 
 Usage: 
 python portfolio.py
 
 (The currencies are hard coded in the script, 
 so are the dates and other parameters.)
  